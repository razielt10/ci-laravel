image: edbizarro/gitlab-ci-pipeline-php:7.4

stages:
  - preparation
  - building
  - syntax
  - testing
  - deploy

# Variables
variables:
  MYSQL_ROOT_PASSWORD: root
  MYSQL_USER: root
  MYSQL_PASSWORD:
  MYSQL_DATABASE: ci_integration
  DB_HOST: mysql

cache:
  key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"

composer:
  stage: preparation
  script:
    - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts --no-suggest
    - cp .env.example .env
    - php artisan key:generate
  artifacts:
    paths:
      - vendor/
      - .env
    expire_in: 1 days
    when: always
  cache:
    paths:
      - vendor/

yarn:
  stage: preparation
  script:
    - yarn install --pure-lockfile
  artifacts:
    paths:
      - node_modules/
    expire_in: 1 days
    when: always
  cache:
    paths:
    - node_modules/
build-dev-assets:
  stage: building
  dependencies:
    - yarn
  script:
    - echo "PUSHER_APP_KEY=$PUSHER_TEST_APP_KEY" >> .env
    - yarn run dev --progress false
  artifacts:
    paths:
      - public/css/
      - public/js/
      - public/fonts/
      - public/mix-manifest.json
    expire_in: 1 days
    when: always
  except:
    - master

build-production-assets:
  stage: building
  dependencies:
    - yarn
  script:
    - echo "PUSHER_APP_KEY=$PUSHER_LIVE_APP_KEY" >> .env
    - yarn run production --progress false
  artifacts:
    paths:
      - public/css/
      - public/js/
      - public/fonts/
      - public/mix-manifest.json
    expire_in: 1 days
    when: always
  only:
    - master

    php-cs-fixer:
      stage: syntax
      dependencies:
        - composer
      script:
        - ./vendor/bin/php-cs-fixer fix --config=.php_cs.php --verbose --diff --dry-run

    eslint:
      stage: syntax
      dependencies:
        - yarn
      script:
        - yarn eslint

phpstan:
  stage: testing
  script:
    - php artisan code:analyse

phpunit:
  stage: testing
  dependencies:
    - composer
  script:
    - php -v
    - php artisan storage:link
    - sudo cp /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.bak
    - echo "" | sudo tee /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
    - ./vendor/phpunit/phpunit/phpunit --version
    - php -d short_open_tag=off ./vendor/phpunit/phpunit/phpunit -v --colors=never --stderr --exclude-group integration
    - sudo cp /usr/local/etc/php/conf.d/docker-php-ext-xdebug.bak /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
  artifacts:
    paths:
      - ./storage/logs
    expire_in: 1 days
    when: on_failure
